#!/bin/bash
FILE1=$1
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";
pandoc "$FILE1" --filter pandoc-citeproc --reference-odt=style.odt -o "$(date +"%Y-%m-%d %R") Manuscript.odt";
#notify-send "ODT written";
