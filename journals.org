#+TITLE: Journals for submission

| Journal name                | Submission date | Last update | Status    |
|-----------------------------+-----------------+-------------+-----------|
| NEJM                        |      2020-02-05 |  2020-02-10 | Rejected  |
| JAMA                        |      2020-02-10 |  2020-02-18 | Rejected  |
| Annals of Internal Medicine |      2020-02-20 |  2020-02-24 | Rejected  |
| medRxiv                     |      2020-02-24 |             | Submitted |
| BMJ Open                    |                 |             |           |
| J Clin Epi                  |                 |             |           |
| Clinical Trials             |                 |             |           |
