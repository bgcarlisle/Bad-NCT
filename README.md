This is the manuscript for an analysis of clinical trial identifiers that were published in abstracts indexed by PubMed, but do not correspond to a legitimate record on ClinicalTrials.gov.
